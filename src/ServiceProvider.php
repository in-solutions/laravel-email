<?php

namespace Insolutions\Email;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        
        $this->publishes([
            __DIR__.'/../db' => base_path('database/sql/insolutions/email'),
            __DIR__.'/../app/Listeners' => app_path('Listeners'),
            __DIR__.'/../resources/views' => base_path('resources/views/insolutions/email'),        
//            __DIR__.'/assets' => public_path('ins/ecommerce'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Insolutions\Email\Controller');
    }
}
