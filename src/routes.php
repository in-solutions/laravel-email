<?php

Route::group(['prefix' => 'email'], function () {
	
	Route::get('send', 'Insolutions\Email\Controller@send');

	Route::get('sendTest', 'Insolutions\Email\Controller@sendTest');
});