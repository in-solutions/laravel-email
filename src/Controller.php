<?php

namespace Insolutions\Email;
 
use Illuminate\Http\Request;

class Controller extends \App\Http\Controllers\Controller
{
    
	public function sendTest(Request $r) {
		Email::send($r->to, "Test email", "Test email content");
	}

	public function send(Request $r) {
    	$emails = Email::whereNull('sent_at')->take(100)->get();

    	foreach ($emails as $email) {
    		echo (Email::api_send($email) ? "OK" : "FAIL") . ' ' . $email->id . PHP_EOL;
    	}        
    }
}