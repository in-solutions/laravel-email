<?php

namespace Insolutions\Email;

use Illuminate\Database\Eloquent\Model;

use Mail;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    //
    protected $table = 't_email_queue';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'sent_at'        
    ];

    protected $casts = [
        'attachments' => 'array'
    ];

    public static function queue($address, $subject, $body, $attachments = []) {
        if (is_array($body)) {
            $tpl = $body[0];
            $data = $body[1];

            $body = (string)view($tpl, (array)$data);
        }

        $email = new self();
        $email->address = $address;
        $email->subject = $subject;
        $email->body = $body;
        $email->attachments = $attachments;

        $email->save();

        return $email;
    }

    public static function send($address, $subject, $body, $attachments = []) {
        $email = self::queue($address, $subject, $body, $attachments);

        return self::api_send($email);
    }
    
    public static function api_send(Email $email) {        
        Mail::send('insolutions.email.emails.echo', ['body' => $email->body], function ($m) use ($email) {
            $m->to($email->address)
                ->subject($email->subject);

            if (is_array($email->attachments) && count($email->attachments) > 0) {
                foreach ($email->attachments as $attachment) {
                    if (isset($attachment['path'])) {
                        $m->attach($attachment['path'], $attachment);
                    } elseif (isset($attachment['url'])) {
                        $data = file_get_contents($attachment['url']);
                        $m->attachData($data, $attachment['as'], $attachment);
                    }
                }
            }
        });

        if(count(Mail::failures()) == 0){
            // no errors
            $email->sent_at = Carbon::now();
            $email->save();
            return true;
        } else {
            error_log('FAILURE');
            return false;
        }
    }
}
